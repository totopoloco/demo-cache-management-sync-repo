package at.mavila.demo.caches.demo.service;

import at.mavila.demo.caches.demo.dto.Item;
import at.mavila.demo.caches.demo.repository.ItemRepository;
import java.time.Instant;
import java.util.List;
import java.util.SplittableRandom;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ItemService {

  private final ItemRepository itemRepository;

  public ItemService(ItemRepository itemRepository) {
    this.itemRepository = itemRepository;
  }

  public List<Item> getAll() {
    log.info("Getting all.");
    return this.itemRepository.findAllOrderById();
  }


  public List<Item> insert() {
    IntStream stream = new SplittableRandom().ints(100, 0, 1_001);
    return stream.mapToObj(operand -> {

      Item item = new Item((long) operand, Boolean.TRUE, RandomStringUtils.randomAlphanumeric(5),
          RandomStringUtils.randomAlphanumeric(5), Instant.now());
      return this.itemRepository.save(item);

    }).toList();
  }
}
